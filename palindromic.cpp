#include "competitive.h"

bool is_palindrome(std::string a) {
    auto mid = a.size() / 2;
    std::string l = a.substr(0, mid);
    if (a.size() % 2 != 0) ++mid;
    std::string r = a.substr(mid);
    std::reverse(std::begin(r), std::end(r));

    return l == r;
}

int main () {
    int T = 0;
    std::cin >> T;

    for (int t = 0; t < T; ++t) {
        std::string input;
        std::cin >> input;

        std::map<char, int> map;
        for (auto c : input) {
            ++map[c];
        }

        std::set<char> odd, even;
        for (auto p : map) {
            if (p.second % 2 == 0) {
                even.insert(p.first);
            } else {
                odd.insert(p.first);
            }
        }

        std::set<std::string> P;
        std::for_each(std::begin(odd), std::end(odd), 
                     [odd, input, &P](char o) {
                         std::string b {input};
                         b.erase(std::remove_if(std::begin(b), std::end(b), 
                                 [o, odd](char c) { 
                                        return odd.find(c) != std::end(odd) && c != o;
                                 }), std::end(b));

                         if (is_palindrome(b)) { P.insert(b); }
                     });

        std::for_each(std::begin(P), std::end(P), 
                     [odd, &P](std::string p) { 
                        p.erase(std::remove_if(std::begin(p), std::end(p),
                                               [odd](char c) {
                                                    return odd.find(c) != std::end(odd);
                                               }), std::end(p));

                        if (is_palindrome(p)) { P.insert(p); }
                     });


        std::vector<std::string> palindromes(std::begin(P), std::end(P));
        std::transform(std::begin(input), std::end(input), 
                       std::back_inserter(palindromes),
                       [](char c) { return std::string {c}; });

        palindromes.erase(std::remove(std::begin(palindromes), std::end(palindromes), ""), 
                                      std::end(palindromes));

        std::cout << palindromes.size() << std::endl;
        for (auto b : palindromes) {
            std::cout << b << std::endl;
        }
    }
}

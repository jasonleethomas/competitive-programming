#include <cstdlib>
#include <cstdio>
#include <map>
#include <vector>

int main() {
    FILE * file = fopen("weather.in.txt", "r+");
    while (true) {
        int N = 0;
        scanf("%d", &N);

        std::map<int, int> data;
        int maybe = 0;
        int max = 0;
        int year_prev = 0;
        for (int n = 0; n < N; ++n) {
            int year = 0, rain = 0;
            scanf("%d %d", &year, &rain);
            //printf("%d %d\n", year, rain);
            if (n > 1) {
                if (year - year_prev > 1) {
                    maybe = 1;
                }
            }

            data[year] = rain;
            year_prev = year;
            
            if (max < rain) {
                max = rain;
            }
        }

        //printf("max %d\n", max);

        int M = 0;
        scanf("%d", &M);
        for (int m = 0; m < M; ++m) {
            int X = 0, Y = 0;
            scanf("%d %d", &Y, &X);
            //printf("%d: %d %d: %d\n", Y, data[Y], X, data[X]); 

            if (maybe) {
                printf("maybe\n");
            }
            else if (data[Y] >= max && data[X] < max) {
                printf("true\n");
            }
            else {
                printf("false\n");
            }
        }

        if (N == 0 && M == 0) 
            break;
    }
}



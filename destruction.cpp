#include "competitive.h"

std::vector<int> M;

void traverse(int n, int m, std::vector<int> h, int D, std::vector<int> & E) {
    if (std::all_of(std::begin(h), std::end(h), [](int v) { return v <= 0; }))
    {
        M.push_back(m);
        return;
    }

    h[n] -= D;    
    std::cout << "attacked: " << n << " " << h[n] << "\n";

    if (h[n] <= 0) {
        int l = n - 1;
        int r = n + 1;

        if (l >= 0) { h[l] -= E[n]; }
        if (r < h.size()) { h[r] -= E[n]; }
    }

    for (int v = 0; v < h.size(); ++v) {
        if (h[v] > 0) {
            traverse(v, m + 1, h, D, E);
        }
    }
}

int main() {    
    int T = 0;
    std::cin >> T;
    for (int t = 0; t < T; ++t) {
        M.clear();

        int D = 0;
        int N = 0;
        std::cin >> N;
        std::cin >> D;

        std::vector<int> H;
        for (int n = 0; n < N; ++n) {
            int h = 0;
            std::cin >> h;
            H.emplace_back(h);
        }

        std::vector<int> E;
        for (int n = 0; n < N; ++n) {
            int e = 0;
            std::cin >> e;
            E.emplace_back(e);
        }

        for (int n = 0; n < N; ++n) {
            traverse(n, 1, H, D, E);
        }

        for (auto m : M) {
            std::cout << m << " ";
        }
    }

    return 0;
}

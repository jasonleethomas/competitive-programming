#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>

int main() {
    int const set_size = 6;
    int set_pieces[] = {1, 1, 2, 2, 2, 8};
    int input[set_size] =  {0};

    for (int i = 0; i < set_size; ++i) {
        std::cin >> input[i];
        std::cout << (set_pieces[i] - input[i]) << " ";
    }

    return 0;
}

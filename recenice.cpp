#include <iostream>
#include <string>
#include <vector>
std::string num_to_string(int);
int main() {
    int N = 0;
    int index = 0;
    int letters = 0;
    std::cin >> N;
    std::vector<std::string> sentence(N);
    for (int i = 0; i < N; i++) {
        std::string word;
        std::cin >> word;

        if (word == "$") {
            index = i;
        } else if (word != "") {
            letters += word.size();
        }

        sentence.push_back(word);
    }

    int needed = letters;

    std::string number_string;
    bool found = false;
    while (!found) {
         found = num_to_string(needed).size() + letters == needed;
         if (!found) 
             needed++; 
    }

    for (int i = 0; i < sentence.size(); ++i) {
        if (sentence[i] == "$")
            sentence[i] = num_to_string(needed);
        if (sentence[i] != "")
            std::cout << sentence[i] << " ";
    }

    return 0;
}

std::string num_to_string(int needed) {
    std::string single[] = {
        "",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten"
    };

    std::string teens[] = {
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
    };

    std::string tens[] = {
        "",
        "",
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety"
    };

    std::string hundreds[] = {
        "",
        "onehundred",
        "twohundred",
        "threehundred",
        "fourhundred",
        "fivehundred",
        "sixhundred",
        "sevenhundred",
        "eighthundred",
        "ninehundred"
    };

    int size = 3;
    int digits[3] = {0};

    for (int i = size - 1; i > 0; --i) {
        int digit = needed % 10;
        needed /= 10;
        digits[i] = digit;
    }

    if (digits[1] != 1) {
        return hundreds[digits[0]] + tens[digits[1]] + single[digits[2]];
    } else {
        return hundreds[digits[0]] + teens[digits[2]];
    }
}

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <iterator>
#include <vector>
#include <queue>


struct Ant {
    int pos;
    int dir;
};

int main(int argc, char * argv[]) {
    std::string filename;
    if (argc < 2) 
       filename = "marching-ants.in.txt";
    else 
       filename = argv[1];

    FILE * input_file = fopen(filename.c_str(), "r+");

    int length = 0;
    int no_ants = 0;
    fscanf(input_file, "%i %i", &length, &no_ants);

    Ant ants[no_ants];
    std::vector<std::queue<Ant *>> platform(length);
    for (int ant = 0; ant < no_ants; ++ant) {
        int pos = 0;
        char dir = 0;
        fscanf(input_file, "%i %c", &pos, &dir);
        ants[ant] = {
            pos,
            dir == 'R'? 1 : -1
        };

        printf("%i %i %i\n", ant, ants[ant].pos, ants[ant].dir);

        // place ant in initial positions
        platform[pos].push(&ants[ant]);
    }

    auto fell_off = [](Ant ant) {
        return ant.pos < 0 || ant.pos > 14;
    };

    int time = 0;
    int no_fell_off = 0;
    int const OFF = -100000000;
    int const EMPTY = -1;

    while (no_fell_off < no_ants) {
        ++time;
        for (int ant = 0; ant < no_ants; ++ant) {
            if (fell_off(ants[ant])) {
                continue;
            }

            int old_pos = ants[ant].pos;
            platform[old_pos].pop();

            int new_pos = (ants[ant].pos += ants[ant].dir);
            if (fell_off(ants[ant])) {
                printf("%i fell at %i\n", ant, time);
                ++no_fell_off;
                continue;
            }

            platform[new_pos].push(&ants[ant]);

            // if occupied change directions
            if (platform[new_pos].size() == 2) {
                platform[new_pos].front()->dir *= -1;
                platform[new_pos].back()->dir *= -1;
            }
        }
    }

    printf("%i\n", time);

    return 0;
}

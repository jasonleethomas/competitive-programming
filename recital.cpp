#include <algorithm>
#include <numeric>
#include <iostream>
#include <vector>
#include <iterator>
#include <limits>
#include <set>

using namespace std;

int matching_characters(string a, string b) {
    sort(begin(a), end(a));
    sort(begin(a), end(a));
    string c;
    set_intersection(begin(a), end(a), begin(b), end(b), back_inserter(c));
    return c.size();
}

int main() {
    int R = 0;
    cin >> R;

    vector<int> ids(R);
    iota(begin(ids), end(ids), 0);

    vector<string> recitals;
    for (int r = 0; r < R; ++r) {
        string recital;
        cin >> recital;
        recitals.push_back(recital);
    }

    vector<vector<int>> map(R, std::vector<int>(R, 0));
    for (int r = 0; r < R; ++r) {
        for (int o = 0; o < R; ++o) {
            map[r][o] = matching_characters(recitals[r], recitals[o]);
        }
    }

    int minimum = std::numeric_limits<int>::max();
    for (int r = 0; r < R; ++r) {
        set<int> not_chosen(begin(ids), end(ids));
        not_chosen.erase(r);
        int prev = r;
        int changes = 0;
        while (!not_chosen.empty()) {
            auto next = min_element(begin(not_chosen), end(not_chosen), 
                           [prev, map](int const & a, int const & b) {
                                return map[prev][a] < map[prev][b];
                           });

            not_chosen.erase(next);
            changes += matching_characters(recitals[*next], recitals[prev]);
            prev = *next;
        }

        if (changes < minimum)
            minimum = changes;
    }

    cout << minimum << endl;
    return 0;
}


#include <algorithm>
#include <limits>
#include <vector>
#include <iterator>
#include <iostream>

using namespace std;

int dist(string a, string b) {
    sort(begin(a), end(a));
    sort(begin(b), end(b));
    string c;
    set_intersection(begin(a), end(a), begin(b), end(b), back_inserter(c));
    return c.size();
}

int main() {
    int R = 0;
    cin >> R;

    vector<string> recitals;
    for (int r = 0; r < R; ++r) {
        string recital;
        cin >> recital;
        recitals.push_back(recital);
    }

    vector<vector<int>> weights(R, vector<int>(R, 0));
    for (int r = 0; r < R; ++r) {
        for (int c = 0; c < R; ++c) {
            weights[r][c] = dist(recitals[r], recitals[c]);
            cout << weights[r][c] << " ";
        }

        cout << "\n"; 
    }


    vector<vector<int>> dp((1 << R), vector<int>(R, -1));
    dp[1][0] = 0;
    for (int i = 1; i < (1 << R); ++i) {
        for (int j = 0; j < R; ++j) {
            if (dp[i][j] == -1) {
                continue;
            }
            for (int k = 1; k < R; ++k) {
                if ((i & (1 << k)) != 0) {
                    continue;
                }
                int p = (i | (1 << k));
                if (dp[p][k] == -1) {
                    dp[p][k] = dp[i][j] + weights[j][k];
                }
                dp[p][k] = min(dp[p][k], dp[i][j] + weights[j][k]);
            }
        }
    }

    int ans = numeric_limits<int>::max();
    for (int r = 1; r < R; ++r) {
        if (dp[(1 << R) - 1][r] > 0) {
            cout << ans << " " <<  dp[(1 << R) - 1][r] + weights[r][0] << endl;
            ans = min(ans, dp[(1 << R) - 1][r] + weights[r][0]);
        }
    }

    cout << ans << endl; 
}

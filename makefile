%: 
	g++ -O0 -g -std=c++11 $@.cpp -o $@

clean:
	find . -name '*.sw*' -delete

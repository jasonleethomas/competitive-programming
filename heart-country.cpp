#include <algorithm>
#include <fstream>
#include <iostream>
#include <utility>
#include <vector>

struct City {
    int troops;
    int visited;
    int captured;
};

std::vector<int> neighbors[1000];
City cities[1000];

int no_enemies = 0;
int total_cities = 0;
int total_troops = 0;

void dfs(int city) {
    cities[city].visited = 1;
    int no_troops = cities[city].troops;
    for (int neighbor : neighbors[city]) {
       no_troops += cities[neighbor].troops;
        if (!cities[neighbor].captured) { // not captured
        }
    }

    if (no_troops <= no_enemies) {
        cities[city].captured = 1; // captured
    } else {
        total_troops += cities[city].troops;
        ++total_cities;
    }

    for (int neighbor : neighbors[city]) { // traverse rest
        if (!cities[neighbor].visited) {
            dfs(neighbor); 
        }
    }
}

int main(int argc, char * argv[]) {
    std::string filename("heart-country.in.txt");
    if (argc > 1) {
        filename = argv[1];
    }

    std::ifstream input_file(filename);
    int no_cities = 0;

    while (true) {
        input_file >> no_cities >> no_enemies;
        printf("%i %i\n", no_cities, no_enemies);

        if (no_cities == 0 && no_enemies == 0) {
            break;
        }
        
        for (int city = 0; city < no_cities; ++city) {
            int no_troops = 0;
            int no_neighbors = 0;
            input_file >> no_troops >> no_neighbors;

            cities[city] = {no_troops, 0, 0};
            for (int neighbor = 0; neighbor < no_neighbors; ++neighbor) {
                int neighbor_city = 0;
                input_file >> neighbor_city;
                neighbors[city].push_back(neighbor_city);
                printf("%i ", neighbors[city][neighbor]);
            }
            printf("\n");
        }

        total_cities = 0;
        total_troops = 0;

        dfs(0);

        printf("\n%i %i\n\n", total_cities, total_troops);
    }

    return 0;
}

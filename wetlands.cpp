#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

using ii = std::pair<int, int>;
using vii = std::vector<ii>;

int const grid_size = 100;
int visited_count = 0;
int visited[grid_size][grid_size];
std::vector<std::string> grid;

bool adjacent(int row, int col) {
 return (grid[row][col] == 'W' && 
         visited[row][col] == 0 && 
         row < grid.size() && 
         col < grid[row].size());
}

void dfs(int x, int y) {
    visited[x][y] = 1; // mark visited
    ++visited_count;
    //printf("->(%i %i), %i\n", x + 1, y + 1, visited_count);
    int d[] = {-1, 0, 1};
    for (int dx : d) {
        for (int dy : d) {
            int row = x + dx;
            int col = y + dy;

            // if adjacent and not visited
            if (adjacent(row, col)) {
                dfs(row, col);
            }
        }
    }
}

int main(int argc, char * argv[]) {
    std::string filename("wetlands.in.txt");
    if (argc > 2) {
        filename = argv[1]; 
    }

    FILE * file = fopen(filename.c_str(), "r");

    int cases = 0;
    char buffer[grid_size], line[grid_size];
    vii points;

    fscanf(file, "%i\n", &cases);
    for (int i = 0; i < cases; ++i) {
        //printf("\ncase: %i\n", i);

        points.clear();
        grid.clear();

        while (fgets(buffer, grid_size * grid_size, file) != NULL) {
            ii point;
            if (sscanf(buffer, "%i %i", &point.first, &point.second) == 2) {
                points.push_back(point);
                //printf("%i %i\n", point.first, point.second);
            } else if (sscanf(buffer, "%s", line) == 1) {
                grid.push_back(line);
                //printf("%s\n", line);
            } else {
                break;
            }
        }

        for (ii point : points) {
            visited_count = 0;
            memset(visited, 0, sizeof(visited[0][0]) * grid_size * grid_size);

            int row = point.first - 1;
            int col = point.second - 1;
            
            if (adjacent(row, col)) {
                dfs(row, col);
            }

            printf("%i\n", visited_count);
        }

        printf("\n");
    }

    return 0;
}

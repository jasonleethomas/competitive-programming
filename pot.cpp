#include <iostream>
#include <string>
#include <cmath>

int main() {
    int X  = 0;
    int N = 0;
    std::string input;
    std::cin >> N;
    for (int i = 0; i < N; ++i) {
        std::cin >> input;
        int P = std::stoi(input.substr(0, input.size() - 1)); 
        int n = std::atoi(&input[input.size() - 1]); 
        int mul = std::pow(P, n);
        X += std::pow(P, n);
    }

    std::cout << X;
    return 0;
}


#include <iostream>

int main() {
    int T;
    std::cin >> T;
    for (int i = 0; i < T; ++i) {
        int prev = -1; 
        int p = -1;

        int bound = 0;
        int sum = 0;
        while (p != 0) {
            std::cin >> p;
            if (prev != -1) {
                bound = (p - 2 * prev);
                sum += bound > 0? bound : 0;
            }

            prev = p;
        }

        std::cout << sum << std::endl;
    }
}

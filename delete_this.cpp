#include <algorithm>
#include <iterator>
#include <iostream>
#include <numeric>
#include <set>
#include <vector>

using namespace std;

enum class Type { begin, end };
enum class Color { black, white };

struct Edge {
    Edge(int id, int value, Type type, Color color) :
        id(id),
        value(value),
        type(type),
        color(color)
    {}

    int id;
    int value;
    Type type;
    Color color;
};

int get_largest_group (vector<Edge> edges) {
    auto compare_value = [](Edge a, Edge b) { return a.value < b.value; };
    sort(begin(edges), end(edges), compare_value);

    //cout << "\nedges\n";
    //for (auto edge : edges) { cout << edge.id << ": " << edge.value << " "; }

    // edge sweep, adding edges in the same region.
    set<set<int>> grouped;
    set<int> active;
    for (auto const & e : edges) {
        switch (e.type) {
        case Type::begin:
            //cout << "\nbegin\n";
            //cout << e.id;
            active.insert(e.id);
            if (active.size() > 1) {
                //cout << "\nactive\n";
                //copy(begin(active), end(active), ostream_iterator<int>(cout, " "));
                grouped.insert(active);
            }
            break;
        case Type::end:
            //cout << "\nend\n";
            //cout << e.id;
            active.erase(e.id);
            break;
        }
    }

    //cout << "\ngrouped\n";
    //for (auto set : grouped) { copy(begin(set), end(set), ostream_iterator<int>(cout, " ")); }

    // all edges with black color
    set<int> blacks;
    auto blacks_end = partition(begin(edges), end(edges), 
                                [](Edge edge) { return edge.color == Color::black; });

    transform(begin(edges), blacks_end, inserter(blacks, begin(blacks)),
              [](Edge edge) { return edge.id; });
    

    // all black edges not yet grouped
    set<int> single_blacks;
    copy_if (begin(blacks), end(blacks), inserter(single_blacks, begin(single_blacks)),
             [grouped](int id) {
                return all_of(begin(grouped), end(grouped), 
                              [id](set<int> group) { return group.find(id) == end(group); });
             });

    //cout << "\nsingle blacks\n";
    //copy(begin(single_blacks), end(single_blacks), ostream_iterator<int>(cout, " "));

    set<set<int>> grouped_blacks;
    transform(begin(single_blacks), end(single_blacks), inserter(grouped_blacks, begin(grouped_blacks)),
              [](int id) { return set<int> { id }; });

    //cout << "\ngrouped blacks\n";
    //for (auto set : grouped_blacks) { copy(begin(set), end(set), ostream_iterator<int>(cout, " ")); }
    
    // copy groups with only blacks
    copy_if(begin(grouped), end(grouped), inserter(grouped_blacks, begin(grouped_blacks)),
           [blacks](set<int> group) {
                return all_of(begin(group), end(group), 
                              [blacks](int id) { return blacks.find(id) != end(blacks); });                
           });

    auto largest_group = max_element(begin(grouped_blacks), end(grouped_blacks), 
                                     [](set<int> a, set<int> b) { return a.size() < b.size(); });

    //cout << "\ngrouped blacks\n";
    //for (auto set : grouped_blacks) { copy(begin(set), end(set), ostream_iterator<int>(cout, " ")); }

    return largest_group != end(grouped_blacks)? largest_group->size() : 0;
}

int main () {
    auto R = 0, 
         C = 0, 
         N = 0, 
         M = 0;

    cin >> R;
    cin >> C;
    cin >> N;
    cin >> M;

    vector<Edge> horizontal, vertical;

    int const width = 9;
    int const height = 15;

    for (auto n = 0; n < N; ++n) {
        auto x = 0,
             y = 0;

        cin >> x;
        cin >> y;

        horizontal.emplace_back( n, x, Type::begin, Color::black );
        horizontal.emplace_back( n, x + width, Type::end, Color::black );

        vertical.emplace_back( n, y, Type::begin, Color::black );
        vertical.emplace_back( n, y + height, Type::end, Color::black );
    }

    for (auto m = N; m < M + N; ++m) {
        auto x = 0,
             y = 0;

        cin >> x;
        cin >> y;

        horizontal.emplace_back( m, x, Type::begin, Color::white );
        horizontal.emplace_back( m, x + width, Type::end, Color::white );

        vertical.emplace_back( m, y, Type::begin, Color::white );
        vertical.emplace_back( m, y + height, Type::end, Color::white );
    }

    auto blacks_to_move = N - max(get_largest_group(horizontal), get_largest_group(vertical));
    cout << blacks_to_move << endl;

    return 0;
}

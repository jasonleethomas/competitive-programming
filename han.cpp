#include <iostream>
#include <fstream>
#include <vector>
#include <map>

int main() {
    std::fstream file("han.in.txt");
    while (!file.eof()) {
        int Q = 0;
        file >> Q;

        int dir = 1;
        int curnum = 0;
        int alphanum[26] = {0};

        for (int q = 0; q < Q; ++q) {
            std::string command = "";
            int N = 0;
            char letter;
            file >> command >> N;

            if (command == "UPIT") {
                file >> letter;
                printf("%s %d %c\n", command.c_str(), N, letter);

            } else {
                printf("%s %d\n", command.c_str(), N);
            } 

            int num = letter % 97;
            for (int n = 0; n < N; ++n) {
                if (curnum < 0)
                    curnum = 25;

                if (curnum > 25)
                    curnum = 0;

                ++alphanum[curnum];
                curnum += dir;

                //printf("%c %d %d\n", curnum, alphanum[curnum], dir);
            }

            if (command == "SMJER") {
                dir *= -1;
                //printf(">%d\n", dir);
            } else {
                printf("%d\n", alphanum[num]);
            }
        }
    }
}

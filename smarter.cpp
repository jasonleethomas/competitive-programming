#include "competitive.h"

struct elephant {
    elephant(int id, int weight, int iq) :
        id(id),
        weight(weight),
        iq(iq) 
    {}

    int id;
    int weight;
    int iq;
};

int main() {
    std::ifstream file { "smarter.in.txt" };
    std::vector<elephant> elephant_data;

    int id = 0;
    while (!file.eof()) {
        auto weight = -1, 
             iq = -1;

        file >> weight;
        file >> iq;

        if (weight != -1 && iq != -1)
            elephant_data.emplace_back(++id, weight, iq);
    }

    auto const compare_weights = [](elephant a, elephant b) {
        return std::tie(a.weight, a.id) < std::tie(b.weight, b.id);   
    };

    std::sort(std::begin(elephant_data), std::end(elephant_data), compare_weights); 
    auto const data_size = elephant_data.size();

    std::vector<int> lis(data_size, 1), parent(data_size, -1);
    auto longest = 0, last = 0;
    for (auto i = 1; i < data_size; ++i) {
        for (auto j = 0; j < i; ++j) {
            if (elephant_data[i].iq < elephant_data[j].iq && lis[i] < lis[j] + 1) {
                lis[i] = lis[j] + 1;
                parent[i] = j;
                if (longest < lis[i]) {
                    longest = lis[i];
                    last = i;
                }
            }
        }
    }

    std::vector<int> sequence(longest);
    for (auto l = longest - 1; l >= 0; --l) {
        sequence[l] = elephant_data[last].id;
        last = parent[last];
    }

    std::cout << longest << std::endl;
    std::copy(std::begin(sequence), std::end(sequence), 
              std::ostream_iterator<int>{std::cout, "\n"});

    return 0;
}

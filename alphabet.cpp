#include <map>
#include <iostream>
#include <string>
#include <vector>

bool in_map(std::map<char, std::string> map, char c) {
    return map.find(c) != std::end(map);
}

int main() {
    std::map<char, std::string> alphamap = {
        {'a', "@"},
        {'b', "8"},
        {'c', "("},
        {'d', "|)"},
        {'e', "3"},
        {'f', "#"},
        {'g', "6"},
        {'h', "[-]"},
        {'i', "|"},
        {'j', "_|"},
        {'k', "|<"},
        {'l', "1"},
        {'m', "[]\\/[]"},
        {'n', "[]\\[]"},
        {'o', "0"},
        {'p', "|D"},
        {'r', "|Z"},
        {'s', "$"},
        {'t', "]["},
        {'u', "|_|"},
        {'v', "\\/"},
        {'w', "\\/\\/"},
        {'x', "}{"},
        {'y', "`/"},
        {'z', "2"}
    };

    std::string input = "";
    std::getline(std::cin, input);

    for (auto input_c : input) {
        char c = input_c;
        if (std::isalpha(input_c))
            c = std::tolower(input_c);

        if (in_map(alphamap, c)) {
            std::cout << alphamap[c];
        } else {
            std::cout << input_c;
        }
    }

    return 0;
}


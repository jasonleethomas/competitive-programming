#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <utility>
#include <vector>

int main(int argc, char * argv[]) {
    std::string filename = "condorcet.in.txt";
    if (argc > 1) {
        filename = argv[1];
    }

    std::ifstream input_file(filename);
    
    int no_ballots = 0;
    int no_candidates = 0;
    int cases = 0;

    while (true) {
        cases++;
        input_file >> no_ballots >> no_candidates;
        if (no_ballots == 0 && no_candidates == 0) {
            break;
        }

        std::vector<int> tally(no_candidates, 0);
        std::pair<int, int> winner = std::make_pair(-1, -1);
        bool tie = false;
        for (int ballot = 0; ballot < no_ballots; ++ballot) {
            for (int vote = 0; vote < no_candidates; ++vote) {
                int candidate;
                input_file >> candidate;
                tally[candidate] += (no_candidates - vote);

                if (tally[candidate] > winner.second) {
                    winner.first = candidate;
                    winner.second = tally[candidate];
                    tie = false;
                }

                if (tally[candidate] == winner.second && 
                    candidate != winner.first) {
                    winner = std::make_pair(-1, -1);
                    tie = true;
                }
            }
        }
        if (winner.first != -1 && !tie) {
            printf("Case %i: %i\n", cases, winner.first);
        } else {
            printf("Case %i: No Condorcet Winner\n", cases);
        }
    }

    return 0;
}

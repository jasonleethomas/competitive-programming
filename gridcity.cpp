#include <iostream>
#include <fstream>
#include <vector>

int main() {
    auto T = 0;
    std::cin >> T ;
    std::cout << T << std::endl;
    for (auto t = 0; t < T; ++t) {
        auto S = 0; 
        auto A = 0;
        auto F = 0;
        std::cin >> S >> A >> F;

        auto sumS = 0, sumA = 0;
        for (auto f = 0; f < F; ++f) {
            auto s = 0;
            auto a = 0;

            std::cin >> s >> a;

            sumS += s;
            sumA += a;
        }

        std::cout << "(Street: " << sumS / F << ", " << "Avenue: " <<  sumA / F << ")" << std::endl;
    }

    return 0;
}

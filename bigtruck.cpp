#include <iostream>
#include <vector>
#include <fstream>
#include <climits>

#define V 101

struct Road {
    int id;
    int weight;
};

std::vector<std::vector<Road>> list (V);
std::vector<int> loads(V);
std::vector<std::vector<int>> paths;
int graph[V][V];

void dfs (int road, int dest, int length, int total, std::vector<int> visited) {
    visited[road] = 1;
    if (road != dest) {
        for (Road neighbor : list[road]) {
            if (!visited[neighbor.id]) {
                dfs (neighbor.id, dest, length + neighbor.weight, total + loads[neighbor.id], visited);
            }
        }
    } else {
        paths.push_back({length, total});
    }
}

//A utility function to print the constructed distance array
void printSolution(int dist[], int n)
{
   printf("Vertex   Distance from Source\n");
   for (int i = 1; i < n; i++) 
       printf("%d \t\t %d\n", i, dist[i]);
}


// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
int minDistance(int dist[], bool sptSet[])
{
   // Initialize min value
   int min = INT_MAX;
   int min_index;
   for (int v = 1; v < V + 1; v++) {
       if (sptSet[v] == false && dist[v] <= min) {
           min = dist[v];
           min_index = v;
       }
   }
   return min_index;
}
  
void dijkstra(int graph[V][V], int dist[V], int src)
{
     bool sptSet[V]; // sptSet[i] will true if vertex i is included in shortest
                     // path tree or shortest distance from src to i is finalized
  
     std::vector<int> parent(V);

     // Initialize all distances as INFINITE and stpSet[] as false
     for (int i = 1; i < V + 1; i++)
        dist[i] = INT_MAX, sptSet[i] = false;
  
     // Distance of source vertex from itself is always 0
     dist[src] = 0;
  
     // Find shortest path for all vertices
     for (int count = 1; count < V; count++)
     {
       // Pick the minimum distance vertex from the set of vertices not
       // yet processed. u is always equal to src in first iteration.
       int u = minDistance(dist, sptSet);
       sptSet[u] = true;

       // Update dist value of the adjacent vertices of the picked vertex.
       for (int v = 1; v < V + 1; v++)
  
         // Update dist[v] only if is not in sptSet, there is an edge from 
         // u to v, and total weight of path from src to  v through u is 
         // smaller than current value of dist[v]
         if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX 
                                       && dist[u]+graph[u][v] < dist[v]) {
                dist[v] = dist[u] + graph[u][v];
                parent[v] = u;
         }
     }
}

int main(int argc, char ** argv) {
    std::ifstream file(argv[1]);

    int N;
    file >> N;
    for (int i = 1; i < N + 1; ++i) {
        int load = 0;
        file >> load;
        loads[i] = load;
    } 


    int roads;
    file >> roads;
    for (int road = 1; road < roads + 1; ++road) {
        int beg, end, weight;
        file >> beg;
        file >> end;
        file >> weight;

        list[beg].push_back({end, weight});
        graph[beg][end] = weight;
    }

    int dist[V] = {0};
    dijkstra(graph, dist, 1);
    printSolution(dist, roads);

    return 0;
}


#include <vector>
#include <iostream>
#include <map>

using namespace std;

int main() {
    int penalty = 20;
    int minutes;
    char problem;
    string right;

    cin >>  minutes;

    map<char, int> scores;
    vector<char> solved;

    while (minutes != -1) {
        cin >> problem;
        cin >> right; 

        if (right == "right") {
            scores[problem] += minutes;
            solved.push_back(problem);
        } else {
            scores[problem] += penalty;
        }

        cin >>  minutes;
    }

    int total = 0;
    for (auto problem : solved) {
        total += scores[problem];
    }

    cout << solved.size() << " " << total << endl;

}

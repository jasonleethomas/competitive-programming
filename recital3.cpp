#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int dist(string a, string b) {
    sort(begin(a), end(a));
    sort(begin(b), end(b));
    string c;
    set_intersection(begin(a), end(a), begin(b), end(b), back_inserter(c));
    return c.size();
}

vector<vector<int>> map(10, vector<int>(10, 0));
vector<int> totals;

void dfs(int r, int changes, vector<int> visited) {
    visited[r] = 1;

    if (!all_of(begin(visited), end(visited), [](int v) { return v == 1; })) {
        for (int i = 0; i < visited.size(); ++i) {
            if (!visited[i] && i != r) {
                dfs(i, changes + map[i][r], visited);
            }
        }
    } else {
        totals.push_back(changes);
    }
}

using namespace std;
int main() {
    int R;
    cin >> R;

    vector<string> recitals; 
    for (int i = 0; i < R; ++i) {
        string r;
        cin >> r;
        recitals.push_back(r);
    }

    for (int i = 0; i < R; ++i) {
        for (int j = 0; j < R; ++j) {
            map[i][j] = dist(recitals[i], recitals[j]);
        }
    }

    for (int i = 0; i < R; ++i) {
        vector<int>  visited(R, 0);
        dfs(i, 0, visited);
    }

    cout << *min_element(begin(totals), end(totals)) << endl;
}

